# RSA
## Teoria
RSA fa parte degli algoritmi di crittografia asimmetrica. É un cifrario a chiave pubblica che permette di cifrare un messaggio attraverso un procedimento che sfrutta le proprietà dei numeri primi. Supponiamo di avere come corrispondenti A e B.
Di solito questo algoritmo viene usato con dei numeri primi di almeno 512 bit cioè almeno 155 cifre. 
## Procedimento per generare le chiavi pubbliche e private
- si generano due numeri primi p e q
- si calcola n che è la moltiplicazione fra i due → n=p*q
- si calcola b con la funzione di eulero → b= (p-1)*(q-1)
- si calcola il primo numero e che sia primo con b (MCD(e, b) = 1)
- si calcola d che è l’inverso di e → cioè in modo che (d*e) mod b == 1
la chiave pubblica è composta da (e; n)
la chiave privata è composta da (d; n)
## Procedimento per criptare un messaggio
per criptare un messaggio bisogna essere in possesso della chiave pubblica. quindi si eleva il messaggioe e si fa il modulo con n → messaggioe mod n = messaggio criptato
## Procedimento per decriptare un messaggio
per decriptare un messaggio bisogna essere in possesso della chiave privata quindi si eleva il messaggio codificatod e fare il modulo con n → messaggio codificatod mod n = messaggio decriptato
## N.B in RSA
si può notare che per i valori da 0 a n-1 i messaggi cifrati sono una permutazione dei messaggi da cifrare. Inoltre si può notare che alcuni valori sono autocifranti ossia hanno lo stesso valore da cifrati. questo accade principalmente con 0 e 1 ma se usiamo come p e q rispettivamente 3 e 5 anche il 4, il 6, il 9, il 10, l’11 e il 14 lo sono. Usando l’RSA con p e q grandi questo problema non interferisce con la sicurezza.
## Sitografia
- **teoria**
    - [edutecnica: teoria](https://www.edutecnica.it/informatica/critto/critto.htm)
    - [crittologia: teoria](http://www.crittologia.eu/critto/rsa/metodo.html)
    - [crittologia: RSA è una permutazione](http://www.crittologia.eu/critto/rsa/rsa_permuta.phtml?num1=3&num2=5&Apply=+Cifra+&Tot=)

- **esercizi**
    - [edutecnica](http://www.edutecnica.it/informatica/crittox/crittox.htm)
- **test**
    - [crittologia](http://www.crittologia.eu/critto/rsa/rsa_demo.phtml?num1=3&num2=5&chiaro=3&Apply=+Cifra+&Tot=)
