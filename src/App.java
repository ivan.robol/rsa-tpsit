import java.util.ArrayList;
import java.util.Scanner;

public class App {
    // http://www.crittologia.eu/mate/prodotto_resto_cinese.phtml --> teorema cinese per la risoluzione 
    private static int e = 2;
    private static int d = 2;
    private static int n = 2;
    private static int b;
    private static double[] risoluzioni;

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int p = 0, q = 0;
        boolean pq = true;
        while (pq == true) {
            System.out.println("inserire p");
            p = sc.nextInt();
            if (primo(p)) {
                pq = false;
            } else {
                System.out.println("p deve essere un numero primo");
            }
        }
        pq = true;
        while (pq == true) {
            System.out.println("inserire q");
            q = sc.nextInt();
            if (q != p && primo(q)) {
                pq = false;
            } else {
                System.out.println("q deve essere diverso da p e deve essere un numero primo");
            }
        }
        // calcolo n=p*q
        n = p * q;
        risoluzioni = new double[n];
        // calcolo b=(p-1)*(q-1)
        b = (p - 1) * (q - 1);
        boolean trovato = false;
        // calcolo e
        while (trovato == false) {
            System.out.println("vuoi generare e in automatico o voi inserirlo? 1 per automatico 2 per inserimento");
            int sceltaE = sc.nextInt();
            if (sceltaE == 1) {
                eAutomatico();
                trovato = true;
            } else if (sceltaE == 2) {
                eManuale();
                trovato = true;
            } else {
                System.out.println("valore non valido, riprova");
            }
        }
        trovato = false;
        // calcolo d
        while (d < Integer.MAX_VALUE && trovato == false) {
            if ((d * e) % b == 1) {
                trovato = true;
            } else {
                d++;
            }
        }
        System.out.println("chiave pubblica --> (" + e + "; " + n + ")\nchiave privata --> (" + d + "; " + n + ")");

        for (int i = 0; i < p * q; i++) {
            risoluzioni[i] += cripta(i);
        }

        boolean finito = false;
        int messaggio;
        while (finito == false) {
            System.out.println("Cosa vuoi fare:\n\t1: criptare\n\t2: decriptare\n\t0: esci");
            int scelta = sc.nextInt();
            if (scelta == 1) {
                System.out.println("inserire il messaggio numerico da cifrare");
                messaggio = sc.nextInt();
                if (messaggio < n) {
                    System.out.println("il messaggio cifrato di " + messaggio + " è: " + cripta(messaggio));
                } else {
                    System.out.println(
                            "il numero è troppo alto per questo programma, prova con un numero minore di " + n);
                }
            } else if (scelta == 2) {
                System.out.println("inserire il messaggio numerico da decifrare");
                messaggio = sc.nextInt();
                if (messaggio < n) {
                    System.out.println("il messaggio decifrato è: " + decripta(messaggio));
                } else {
                    System.out.println(
                            "il numero è troppo alto per questo programma, prova con un numero minore di " + n);
                }
            } else if (scelta == 0) {
                finito = true;
            } else {
                System.out.println("valore non valido, riprova");
            }
        }
        sc.close();
    }

    public static int mcd(int a, int b) {
        if (a == b)
            return a;
        else if (a > b)
            return mcd(a - b, b);
        else
            return mcd(b, a);
    }

    public static double cripta(int messaggio) {
        return Math.pow(messaggio, e) % n;
    }

    /*
     * public static long decripta(int messaggio){ non funziona perché messaggio^d è
     * un numero troppo grande
     * return (long) Math.pow(messaggio, d)%n;
     * }
     */

    public static int decripta(int messaggio) {
        boolean trovato = false;
        int i = 0;
        while (trovato == false && i < risoluzioni.length) {
            if (risoluzioni[i] == messaggio) {
                trovato = true;
            } else {
                i++;
            }
        }
        return i;
    }

    public static void eAutomatico() {
        boolean trovato = false;
        while (e < 10000 && trovato == false) {
            if (mcd(e, b) == 1) {
                trovato = true;
            } else {
                e++;
            }
        }
    }

    public static void eManuale() {
        Scanner sc1 = new Scanner(System.in);
        boolean trovato = false;
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        while (e < 10000) {
            if (mcd(e, b) == 1) {
                tmp.add(e);
            } // else {
            e++;
            // }
        }
        trovato = false;
        int scelta;
        e = Integer.MAX_VALUE;
        while (trovato == false) {
            System.out.println("inserire e");
            scelta = sc1.nextInt();
            for (int i = 0; i < tmp.size(); i++) {
                if (tmp.get(i) == scelta) {
                    e = scelta;
                    trovato = true;
                }
            }
            if (e == Integer.MAX_VALUE) {
                System.out.println("il valore selezionato non fa parte di quelli possibili");
            }
        }
    }

    public static boolean primo(int num) {
        int i = 2;
        boolean primo = true;
        while (i < num && primo == true) {
            if (num % i != 0) {
                primo = true;
            }else{
                primo=false;
            }
            i++;
        }
        return primo;
    }
}
    